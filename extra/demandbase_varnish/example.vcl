/**
 * EXAMPLE VCL FOR INTEGRATION WITH DEMANDBASE_VARNISH MODULE
 * ==========================================================
 *
 * This file contains 4 custom subroutines, and then some example VCL that makes
 * use of them, calling from approprioate places in vcl_recv and vcl_deliver.
 */


/**
 * SUBROUTINES
 *
 * The subroutines are for calling from within your standard VCL config, and
 * provide the necessary logic for working with the demandbase_varnish Drupal
 * module. Each subroutine should be placed as shown in the following example
 * vcl file. They are listed here in logical order (eg the order you would
 * expect them to run for a first time visitor), because the logic can be hard
 * to follow when jumping from vcl_recv to vcl_deliver, back again, and then
 * back to vcl_deliver a second time. Yes, it's a bit wierd.
 *
 * The process is as follows:
 *
 * 1. For first time visitors (with no cookie set), override the request url to
 *    instead hit a special demandbase cookie-callback in Drupal.
 * 2. Scrape the returned cookie value from Drupals response, add it to the
 *    request, and tell Varnish to start over again.
 * 3. Restore the originally requested url and then continue as per usual, with
 *    the cookie now set on the request, as if the users browser had sent with
 *    the original request.
 * 4. To save all this hoop-jumping on subsequent requests, add a SetCookie
 *    header to the response to actually set the cookie in the users
 *    browser. That way, next time it's just a straight cache hit.
 * 
 * The key thing it all hinges off is the 'X-Demandbase-Cookie-Stage' 
 * flag we set as a custom HTTP header in the first step, and then update 
 * and propogate in each step.
 */

/**
 * 1. Monkey with incoming requests that don't have a cookie.
 *
 * This needs to be called near the bottom of vcl_recv, after other cookie logic
 * has worked.
 *
 * We change the request url to a lightweight cookie-callback url, that will be
 * responsible for including the users demandbase cookie value as custom header.
 * We'll make use of that in step 2, called in in vcl_deliver.
 */
sub demandbase_step_1 {
  // If the request doesn't have a demandbase cookie on it
  if (req.restarts == 0 && req.http.Cookie !~ "demandbase=") {
    // ...and isn't a static asset
    if (req.url !~ "(?i)\.(pdf|asc|dat|txt|doc|xls|ppt|tgz|csv|png|gif|jpeg|jpg|ico|swf|css|js|html|htm)(\?.*)?$") {
      // Save away the original request url, we'll restore this in step 3.
      set req.http.X-Demandbase-Original-URL = req.url;
      // Redirect the request to the demandbase cookie-callback.
      set req.url = "/js/demandbase_varnish/get_cookie";
      // Set a flag to let our code in vcl_deliver know what we're up to.
      set req.http.X-Demandbase-Cookie-Stage = "retrieval";
      // Don't try and get it from cache.
      return (pass);
    }
  }
}

/**
 * 2. Add the returned cookie value to the request, and start again.
 *
 * This needs to be placed in vcl_deliver.
 *
 * The cookie callback response includes a couple of custom HTTP headers:
 *   1. X-Demandbase-Varnish-Cookie
 *      The is the demandbase cookie value for the user, which we attach to our
 *      request so that on the second pass through our VCL it's as if the user
 *      had sent it with the original request.
 *   2. X-Demandbase-Varnish-Cookie-Set
 *      This is a correctly formatted string, for use as a Set-Cookie response
 *      header. We'll attach this to our response to the browser, so that the
 *      cookie we've just grabbed from Drupal will actually get set in the users
 *      browser and thus already be there on subsequent requests.
 */
sub demandbase_step_2 {
  // Respond to the flag we set back in step 1.
  if (req.http.X-Demandbase-Cookie-Stage == "retrieval") {
    // Check our cookie-callback actually returned the headers we expect.
    if (resp.http.X-Demandbase-Varnish-Cookie && resp.http.X-Demandbase-Varnish-Cookie-Set) {
      // Play nicely with other existing cookies on the request.
      if (req.http.Cookie) {
        set req.http.X-Tmp = req.http.Cookie + ";";
      }
      else {
        set req.http.X-Tmp = "";
      }
      // Append our demandbase cookie value onto the request.
      set req.http.Cookie = req.http.X-Tmp + resp.http.X-Demandbase-Varnish-Cookie;
      unset req.http.X-Tmp;
      // Save away the 'Set-Cookie' string, as we'll need to add that to the
      // response the second time we hit vcl_deliver after restarting.
      set req.http.X-Demandbase-Varnish-Cookie-Set = resp.http.X-Demandbase-Varnish-Cookie-Set;
    }
    // Update the progress flag, so our code in vcl_recv knows where we're up
    // to.
    set req.http.X-Demandbase-Cookie-Stage = "received";
    // Tell varnish to start over (but now with a demandbase cookie on the
    // request!)
    return (restart);
  }
}

/**
 * 3. Restore the originally requested url.
 *
 * This should be called at the top of vcl_recv, so that any following logic is
 * based on the original url we restore here.
 */
sub demandbase_step_3 {
  // Respond to our flag from step 3.
  // It tells us we're on our second pass, after first obtaining the demandbase 
  // cookie from our Drupal cookie-callback.
  if (req.restarts > 0 && req.http.X-Demandbase-Cookie-Stage == "received") {
    // Restore the original URL.
    set req.url = req.http.X-Demandbase-Original-URL;
    unset req.http.X-Demandbase-Original-URL;
    // Update our flag, so code in vcl_deliver knows where we're up to.
    set req.http.X-Demandbase-Cookie-Stage = "done";
  }

  // This is just some housekeeping. We unset internal headers only applicable
  // to requests we've restarted. Otherwise cheeky users could send requests
  // including them and strange things would happen.
  if (req.restarts == 0) {
    unset req.http.X-Demandbase-Cookie-Stage;
    unset req.http.X-Demandbase-Original-URL;
    unset req.http.X-Demandbase-Varnish-Cookie;
    unset req.http.X-Demandbase-Varnish-Cookie-Set;
  }
}

/**
 * 4. Set the cookie in the users browser.
 *
 * This needs to be called from vcl_deliver.
 */
sub demandbase_step_4 {
  // Respond to the flag we set in step 3.
  if (req.http.X-Demandbase-Cookie-Stage == "done") {
    // Set the Set-Cookie header to the string we got back from our Drupal
    // cookie-callack.
    if (req.http.X-Demandbase-Varnish-Cookie-Set) {
      set resp.http.Set-Cookie = req.http.X-Demandbase-Varnish-Cookie-Set;
    }
  }
}




/**
 * EXAMPLE VCL
 *
 * Whilst this would work on it's own, it's primarily to demonstrate where to
 * call the sub routines from. YMMV. The key bits to take note of are the fact
 * that we're explicitly not stripping out the demandbase cookie, and are opting
 * out of the default vcl_recv with our return(lookup) line at the bottom. The
 * default vcl_recv assumes any requests with cookies are uncacheable
 * means Varnish
 */
backend default {
  .host = "127.0.0.1";
  .port = "8080";
  .connect_timeout = 600s;
  .first_byte_timeout = 600s;
  .between_bytes_timeout = 600s;
}
sub vcl_recv {
  call demandbase_step_3;

  set req.grace = 6h;

  # Do not cache these paths.
  if (req.url ~ "^/status\.php$" ||
      req.url ~ "^/update\.php$" ||
      req.url ~ "^/ooyala/ping$" ||
      req.url ~ "^/admin/build/features" ||
      req.url ~ "^/info/.*$" ||
      req.url ~ "^/flag/.*$" ||
      req.url ~ "^/admin/.*$" ||
      req.url ~ "^.*/ajax/.*$" ||
      req.url ~ "^/js/.*$" ||
      req.url ~ "^.*/ahah/.*$") {
       return (pass);
  }

  if (req.request != "GET" &&
    req.request != "HEAD" &&
    req.request != "PUT" &&
    req.request != "POST" &&
    req.request != "TRACE" &&
    req.request != "OPTIONS" &&
    req.request != "DELETE") {
      /* Non-RFC2616 or CONNECT which is weird. */
      return (pipe);
  }
  if (req.request != "GET" && req.request != "HEAD") {
    /* We only deal with GET and HEAD by default */
    return (pass);
  }

  # Ignore cookies for static asset requests. They won't vary by cookie.
  if (req.url ~ "(?i)\.(pdf|asc|dat|txt|doc|xls|ppt|tgz|csv|png|gif|jpeg|jpg|ico|swf|css|js|html|htm)(\?.*)?$") {
    unset req.http.Cookie;
  }

  # Handle compression correctly. Different browsers send different
  # "Accept-Encoding" headers, even though they mostly all support the same
  # compression mechanisms. By consolidating these compression headers into
  # a consistent format, we can reduce the size of the cache and get more hits.=
  # @see: http:// varnish.projects.linpro.no/wiki/FAQ/Compression
  if (req.http.Accept-Encoding) {
    if (req.http.Accept-Encoding ~ "gzip") {
      # If the browser supports it, we'll use gzip.
      set req.http.Accept-Encoding = "gzip";
    }
    else if (req.http.Accept-Encoding ~ "deflate") {
      # Next, try deflate if it is supported.
      set req.http.Accept-Encoding = "deflate";
    }
    else {
      # Unknown algorithm. Remove it and send unencoded.
      unset req.http.Accept-Encoding;
    }
  }

  # Remove all cookies that Drupal doesn't need to know about. ANY remaining
  # cookie will cause the request to pass-through to Apache. For the most part
  # we always set the NO_CACHE cookie after any POST request, disabling the
  # Varnish cache temporarily. The session cookie allows all authenticated users
  # to pass through as long as they're logged in.
  if (req.http.Cookie) {
    set req.http.Cookie = ";" +req.http.Cookie;
    set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
    set req.http.Cookie = regsuball(req.http.Cookie, ";(has_js|S{1,2}ESS[a-z0-9]+|NO_CACHE|nocache|demandbase)=", "; \1=");
    set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

    if (req.http.Cookie == "") {
      # If there are no remaining cookies, remove the cookie header. If there
      # aren't any cookie headers, Varnish's default behavior will be to cache
      # the page.
      unset req.http.Cookie;
    }
    # If there is a session cookie or the NO_CACHE cookie, do not cache the page.
    elsif (req.http.Cookie ~ "(S{1,2}ESS[a-z0-9]+|NO_CACHE|nocache)=") {
      return (pass);
    }
  }


  # We deliberately ignore cookies here, we pass on the ones we care about.
  if (req.http.Authorization) {
      /* Not cacheable by default */
      return (pass);
  }

  call demandbase_step_1;

  # And we deliberately don't fall through to the default handling.
  return (lookup);
}


sub vcl_deliver {
  call demandbase_step_2;
  call demandbase_step_4;
}
