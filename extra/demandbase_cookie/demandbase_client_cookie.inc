<?php

class demandbase_client_cookie extends demandbase_client {

  protected function is_ip_self($requested_ip = '') {
    $overridden = (!empty($this->session_ip_address) && $this->activate_visitor_ip_address);
    return !$overridden && (empty($requested_ip) || $requested_ip == $this->ip_address);
  }

  /**
   * Retrieve visitor information from demandbase.
   *
   * If possible, utilise cookie before/after making API call.
   *
   * @param String $field
   *   return this field
   * @param String $ip
   *   process based on the ip information passed
   */
  public function fetch_data($field = 'ip', $ip = '', $force_fetch = FALSE) {
    $response = '';
    if (!$this->is_ip_self($ip) || !$this->hasCookie() || $force_fetch) {
      $response = parent::fetch_data($field, $ip);
      if ($this->is_ip_self($ip)) {
        $this->setCookie();
      }
    }
    else {
      $this->processed_data = $this->getCookie();
      if ($field == 'ip') {
        $response = $this->ip_address;
      }
      else {
        $response = $this->getFromCookie($field);
      }
    }
    return $response;
  }

  protected function setCookie() {
    // We need to get the values set for each of the conditions on each of the
    // contexts. Then we can determine whether they're applicable or not.
    $contexts = context_load();
    $context_conditions = context_conditions();
    $demandbase_conditions_info = $context_conditions['demandbase_context_conditions'];
    $condition_values = array();
    foreach ($contexts as $context) {
      $conditions_object = new demandbase_context_conditions($context, $demandbase_conditions_info);
      $values = $conditions_object->get_configured_conditions(array_keys(demandbase_get_specified_conditions()));
      foreach($values as $name => $value) {
        $condition_values[$name][] = $value;
      }
    }

    // Go through and set cookie values only for contexts that might apply.
    // If no context applies, then set a generic cookie value for this property.
    // This makes our caching significantly more effective, since we don't cache
    // every unused variant of each context condition.
    foreach(demandbase_get_specified_conditions() as $key => $name) {
      // Check that we actually have a value for this property.
      if (!empty($this->processed_data[$key])) {
        foreach ($condition_values as $condition => $value) {
          // Check whether any conditions apply, then add the keys.
          if ($this->match(drupal_strtolower($this->processed_data[$key]), $condition_values[$key])) {
            $cookie_array[$key] = !empty($this->processed_data[$key]) ? $this->processed_data[$key] : '';
          }
          else {
            $cookie_array[$key] = '';
          }
        }
      }
      else {
        $cookie_array[$key] = '';
      }
    }

    $cookie_string = $this->encodeCookieArray($cookie_array);
    // Set the cookie for the rest of this request.
    $_COOKIE[DEMANDBASE_COOKIE] = $cookie_string;
    // Actually send the cookie with our response.
    $expires = REQUEST_TIME + (60 * 60 * 24);
    setcookie(DEMANDBASE_COOKIE, $cookie_string, $expires, base_path(), '', FALSE, TRUE);
  }

  protected function getCookie() {
    if ($this->hasCookie()) {
      $cookie_array = $this->decodeCookieArray($_COOKIE[DEMANDBASE_COOKIE]);
      return $cookie_array;
    }
  }

  protected function hasCookie() {
    if (isset($_COOKIE) && isset($_COOKIE[DEMANDBASE_COOKIE])) {
      $cookie_array = $this->decodeCookieArray($_COOKIE[DEMANDBASE_COOKIE]);
      if (!empty($cookie_array)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  protected function issetInCookie($field) {
    if ($this->hasCookie()) {
      $cookie_array = $this->getCookie();
      if (isset($cookie_array[$field])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  protected function getFromCookie($field) {
    if ($this->issetInCookie($field)) {
      $cookie_array = $this->getCookie();
      return $cookie_array[$field];
    }
  }

  protected function encodeCookieArray($array) {
    ksort($array);
    return drupal_json_encode($array);
  }

  protected function decodeCookieArray($string) {
    return drupal_json_decode($string);
  }
}